package com.education.cloud.system.feign.interfaces;

import com.education.cloud.system.feign.qo.SysRoleUserQO;
import com.education.cloud.system.feign.vo.SysRoleUserVO;
import com.education.cloud.util.base.Page;
import com.education.cloud.util.constant.ServiceConstant;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 角色用户关联表
 *
 * @author wujing
 */
@FeignClient(name = ServiceConstant.SYSTEM_SERVICE,contextId = "sysRoleUserClient")
public interface IFeignSysRoleUser {


    @RequestMapping(value = "/feign/system/sysRoleUser/listForPage")
    Page<SysRoleUserVO> listForPage(@RequestBody SysRoleUserQO qo);

    @RequestMapping(value = "/feign/system/sysRoleUser/save")
    int save(@RequestBody SysRoleUserQO qo);

    @RequestMapping(value = "/feign/system/sysRoleUser/deleteById")
    int deleteById(@RequestBody Long id);

    @RequestMapping(value = "/feign/system/sysRoleUser/updateById")
    int updateById(@RequestBody SysRoleUserQO qo);

    @RequestMapping(value = "/feign/system/sysRoleUser/getById")
    SysRoleUserVO getById(@RequestBody Long id);

}
