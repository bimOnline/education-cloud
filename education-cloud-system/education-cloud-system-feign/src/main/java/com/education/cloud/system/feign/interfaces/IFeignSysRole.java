package com.education.cloud.system.feign.interfaces;

import com.education.cloud.system.feign.qo.SysRoleQO;
import com.education.cloud.system.feign.vo.SysRoleVO;
import com.education.cloud.util.base.Page;
import com.education.cloud.util.constant.ServiceConstant;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 角色信息
 *
 * @author wujing
 */
@FeignClient(name = ServiceConstant.SYSTEM_SERVICE,contextId = "sysRoleClient")
public interface IFeignSysRole {

    @RequestMapping(value = "/feign/system/sysRole/listForPage")
    Page<SysRoleVO> listForPage(@RequestBody SysRoleQO qo);

    @RequestMapping(value = "/feign/system/sysRole/save")
    int save(@RequestBody SysRoleQO qo);

    @RequestMapping(value = "/feign/system/sysRole/deleteById")
    int deleteById(@RequestBody Long id);

    @RequestMapping(value = "/feign/system/sysRole/updateById")
    int updateById(@RequestBody SysRoleQO qo);

    @RequestMapping(value = "/feign/system/sysRole/getById")
    SysRoleVO getById(@RequestBody Long id);

}
