package com.education.cloud.system.service.feign.biz;

import com.education.cloud.system.service.dao.SysUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.education.cloud.system.feign.qo.SysUserQO;
import com.education.cloud.system.feign.vo.SysUserVO;
import com.education.cloud.system.service.dao.impl.mapper.entity.SysUser;
import com.education.cloud.system.service.dao.impl.mapper.entity.SysUserExample;
import com.education.cloud.system.service.dao.impl.mapper.entity.SysUserExample.Criteria;
import com.education.cloud.util.base.Page;
import com.education.cloud.util.base.PageUtil;
import com.education.cloud.util.tools.BeanUtil;

/**
 * 后台用户信息
 *
 * @author wujing
 */
@Component
public class FeignSysUserBiz {

	@Autowired
	private SysUserDao dao;

	public Page<SysUserVO> listForPage(SysUserQO qo) {
		SysUserExample example = new SysUserExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<SysUser> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, SysUserVO.class);
	}

	public int save(SysUserQO qo) {
		SysUser record = BeanUtil.copyProperties(qo, SysUser.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public SysUserVO getById(Long id) {
		SysUser record = dao.getById(id);
		return BeanUtil.copyProperties(record, SysUserVO.class);
	}

	public int updateById(SysUserQO qo) {
		SysUser record = BeanUtil.copyProperties(qo, SysUser.class);
		return dao.updateById(record);
	}

}
