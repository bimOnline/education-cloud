package com.education.cloud.course.feign.interfaces;

import com.education.cloud.course.feign.qo.CourseQO;
import com.education.cloud.course.feign.vo.CourseVO;
import com.education.cloud.util.base.Page;
import com.education.cloud.util.constant.ServiceConstant;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 课程信息
 *
 * @author wujing
 */
@FeignClient(name = ServiceConstant.COURSE_SERVICE,contextId = "courseClient")
public interface IFeignCourse {

    @RequestMapping(value = "/feign/course/course/listForPage", method = RequestMethod.POST)
    Page<CourseVO> listForPage(@RequestBody CourseQO qo);

    @RequestMapping(value = "/feign/course/course/save", method = RequestMethod.POST)
    int save(@RequestBody CourseQO qo);

    @RequestMapping(value = "/feign/course/course/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/feign/course/course/update", method = RequestMethod.PUT)
    int updateById(@RequestBody CourseQO qo);

    @RequestMapping(value = "/feign/course/course/get/{id}", method = RequestMethod.GET)
    CourseVO getById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/feign/course/course/getByCourseId/{id}", method = RequestMethod.GET)
    CourseVO getByCourseId(@PathVariable(value = "id") Long id);

}
