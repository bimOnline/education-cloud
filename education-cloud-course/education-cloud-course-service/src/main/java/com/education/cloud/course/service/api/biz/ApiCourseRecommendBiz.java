package com.education.cloud.course.service.api.biz;

import java.util.ArrayList;
import java.util.List;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.education.cloud.course.common.dto.CourseRecommendDTO;
import com.education.cloud.course.common.dto.CourseRecommendListDTO;
import com.education.cloud.course.service.dao.CourseDao;
import com.education.cloud.course.service.dao.CourseRecommendDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.education.cloud.course.common.bo.CourseRecommendBO;
import com.education.cloud.course.service.dao.impl.mapper.entity.Course;
import com.education.cloud.course.service.dao.impl.mapper.entity.CourseRecommend;
import com.education.cloud.util.base.Result;
import com.education.cloud.util.enums.StatusIdEnum;
import com.education.cloud.util.tools.BeanUtil;


/**
 *
 * 课程推荐
 *
 * @author kyh
 *
 */
@Component
public class ApiCourseRecommendBiz {

	@Autowired
	private CourseDao courseDao;

	@Autowired
	private CourseRecommendDao dao;

	/**
	 * 根据分类ID获取推荐课程信息
	 *
	 * @param courseRecommendBO
	 * @author kyh
	 */
	public Result<CourseRecommendListDTO> list(CourseRecommendBO bo) {
		List<CourseRecommend> list = dao.listByCategoryIdAndStatusId(bo.getCategoryId(), StatusIdEnum.YES.getCode());
		CourseRecommendListDTO dto = new CourseRecommendListDTO();
		if (CollectionUtil.isEmpty(list)) {
			return Result.success(dto);
		}
		List<CourseRecommendDTO> listDTO = new ArrayList<>();
		for (CourseRecommend courseRecommend : list) {
			Course course = courseDao.getById(courseRecommend.getCategoryId());
			if (ObjectUtil.isNotNull(course)) {
				CourseRecommendDTO courseRecommendDTO = BeanUtil.copyProperties(course, CourseRecommendDTO.class);
				listDTO.add(courseRecommendDTO);
			}
		}
		dto.setList(listDTO);
		return Result.success(dto);
	}

}
