package com.education.cloud.course.service.dao;

import com.education.cloud.course.service.dao.impl.mapper.entity.FileStorage;
import com.education.cloud.course.service.dao.impl.mapper.entity.FileStorageExample;
import com.education.cloud.util.base.Page;

public interface FileStorageDao {
    int save(FileStorage record);

    int deleteById(Long id);

    int updateById(FileStorage record);

    FileStorage getById(Long id);

    Page<FileStorage> listForPage(int pageCurrent, int pageSize, FileStorageExample example);
}
