package com.education.cloud.course.service.api.biz;

import java.util.List;

import cn.hutool.core.collection.CollectionUtil;
import com.education.cloud.course.common.bo.AdvBO;
import com.education.cloud.course.service.dao.AdvDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.education.cloud.course.common.dto.AdvDTO;
import com.education.cloud.course.common.dto.AdvListDTO;
import com.education.cloud.course.service.dao.impl.mapper.entity.Adv;
import com.education.cloud.util.base.PageUtil;
import com.education.cloud.util.base.Result;
import com.education.cloud.util.tools.DateUtil;
import com.education.cloud.util.enums.StatusIdEnum;


/**
 * 广告信息
 *
 * @author wujing
 */
@Component
public class ApiAdvBiz {

	@Autowired
	private AdvDao advDao;

	public Result<AdvListDTO> list(AdvBO advBO) {
		AdvListDTO dto = new AdvListDTO();
		// 开始时间和结束时间
		List<Adv> advList = advDao.listByPlatShowAndStatusIdAndBeginTimeAndEndTime(advBO.getPlatShow(), StatusIdEnum.YES.getCode(), DateUtil.parseDate("2019-07-04", "yyyy-MM-dd"), DateUtil.parseDate("2019-07-03", "yyyy-MM-dd"));
		if (CollectionUtil.isNotEmpty(advList)) {
			dto.setAdvList(PageUtil.copyList(advList, AdvDTO.class));
		}
		return Result.success(dto);
	}

}
